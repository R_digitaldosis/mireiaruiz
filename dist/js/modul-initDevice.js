import { calcWinsize } from './modul-utils.js'
// Get Device
const mediaQuery = function(query) {
    var breakpoints = {
    mobile: '(min-width: 320px)',
    tablet: '(min-width: 768px)',
    desktop: '(min-width: 1024px)',
    retina:
        'only screen and (-webkit-min-device-pixel-ratio:2), only screen and (min--moz-device-pixel-ratio:2), only screen and (min-device-pixel-ratio:2), only screen and (min-resolution:2dppx), only screen and (min-resolution:192dpi)',
    };
    switch (query) {
    case 'mobile':
        return window.matchMedia(breakpoints.mobile).matches && !window.matchMedia(breakpoints.tablet).matches;
    case 'tablet':
        return window.matchMedia(breakpoints.tablet).matches && !window.matchMedia(breakpoints.desktop).matches;
    case 'desktop':
        return window.matchMedia(breakpoints.desktop).matches;
    case 'retina':
        return window.matchMedia(breakpoints.retina).matches;
    default:
        return false;
    }
};
// Get Device
export const getCurrentBreakpoint = function() {
    var current = mediaQuery('mobile') ? 'mobile' : null;
    current = mediaQuery('tablet') ? 'tablet' : current;
    current = mediaQuery('desktop') ? 'desktop' : current;
    return current;
};

// Init Device
class InitDevice {
    constructor() {
        this.device;
        this.size = calcWinsize();

        this.setDevice();
        this.reload = () => {
            let lastDevice = this.device;
            this.setDevice();
            if (lastDevice !== this.device) location.reload();
        }
        window.addEventListener("resize", () => this.reload());
    }
    getSize() {
        return this.size;
    }
    //init() {
    setDevice() {
        this.device = getCurrentBreakpoint() !== 'desktop' ? 'mobile' : 'desktop';
    }
    getDevice() {
        return this.device;
        /* if(this.device === 'desktop'){
            console.log('initAppDesktop');
            return 'desktop';
        }else{
            console.log('initAppMobile');
            return 'mobile';
        } */
    }
}
//Init Device
export const initDevice = new InitDevice();
