class Menu {
    constructor() {
        this.container = document.querySelector('.header__menu');
        this.el = document.querySelector('.header__menu .menu');
        this.btn = document.querySelector('.header__icon-menu-mobile');
        this.item_contact = this.el.querySelector('.menu__item--contact');
        this.contact = document.querySelector('.contact');

        this.activateToggle();
        this.showContact();
    }

    activateToggle() {
        this.btn.addEventListener("click", () => {
            this.container.classList.toggle('is-open');
            this.btn.classList.toggle('is-active');
        })
    }

    showContact() {
        this.item_contact.addEventListener("click", (e) => {
            e.preventDefault();
            const contact_width = this.contact.offsetWidth;
            this.item_contact.classList.toggle('active');
            this.contact.classList.toggle('is-visible');
            let Xtranslate = this.contact.classList.contains('is-visible') ? -(contact_width) : 0;
            gsap.to(this.el, {
                x: Xtranslate,
                duration: .4
            });
        });
    }
}

export const mainMenu = new Menu();