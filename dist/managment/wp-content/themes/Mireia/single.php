<?php get_header(); ?>
<div class="loader loading"></div>
<div class="page-wrapper page-wrapper--single page-wrapper--vertical">
	<div class="random scroll-wrap">
			<?php
			$images = get_field('gallery');
			while ( have_posts() ) : the_post();
				if( $images ): ?>
				<div class="random__featured-container <?php if( count($images) > 1 ):echo 'random__featured-container--left';endif;?>">
					<picture class="random__featured-image random__featured-image--first-load trigger">
						<source srcset="<?=$images[0].'.webp'?>" type="image/webp">
						<source srcset="<?=$images[0]?>" type="image/jpeg"> 
						<img src="<?=$images[0]?>">
					</picture>
					<?php if( count($images) > 1 ){ ?>
						<picture class="random__featured-image random__featured-image--1 random__featured-image--first-load">
							<source srcset="<?=$images[1].'.webp'?>" type="image/webp">
							<source srcset="<?=$images[1]?>" type="image/jpeg"> 
							<img src="<?=$images[1]?>">
						</picture>
					<?php } ?>
				</div>
				<?php
				endif;
				?>
				<div class="random__featured-content">
					<div class="random__featured-column random__featured-column--right">
						<h1><?=get_the_title()?></h1>
						<?=get_field('descripcio')?>
					</div>
					<div class="random__featured-column random__featured-column--left">
						<p><?=get_field('info')?></p>
					</div>
				</div>
			<?php
			if( count($images) > 2 ){ ?>
			<div class="random__featured-container random__featured-container--right">
				<picture class="random__featured-image random__featured-image--2">
					<source srcset="<?=$images[2].'.webp'?>" type="image/webp">
					<source srcset="<?=$images[2]?>" type="image/jpeg"> 
					<img src="<?=$images[2]?>">
				</picture>
			</div>
			<?php }
			if( count($images) > 3 ){ ?>
			<div class="random__featured-container random__featured-container--last">
				<?php 
				$serie = 0;
				for($i=3;$i<count($images);$i++){ ?>
					<picture class="random__featured-image">
						<source srcset="<?=$images[$i].'.webp'?>" type="image/webp">
						<source srcset="<?=$images[$i]?>" type="image/jpeg"> 
						<img src="<?=$images[$i]?>">
					</picture>
				<?php
					$serie++;
					if($serie >= 3){ ?>
						</div>
						<div class="random__featured-container random__featured-container--last">
					<?php
					$serie = 0;
					}
				} ?>
			</div>
			<?php }
			endwhile;
			?>
			<div class="footer__credits credits"><a href="http://digitaldosis.com" target="_blank">Site by DigitalDosis</a></div>
	</div>
	<div class="scroll-icon"></div>
</div>
<svg class="cursor cursor--pointer" width="34.5" height="10" viewBox="0 0 23.77 10">
	<circle class="cursor--pointer__point cursor--pointer__point--works cursor--pointer__point--vertical" cx="3" cy="3" r="3"/>
</svg>
<script>history.scrollRestoration = "manual"</script>
<?php get_footer();