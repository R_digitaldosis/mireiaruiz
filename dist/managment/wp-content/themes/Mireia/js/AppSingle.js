import { utils } from './modul-utils.js';
import { InitDevice } from './modul-initDevice.js';
import SmoothScroll from './modul-scroll.js';

class Work {
    constructor(el) {
        this.DOM = {el: el};
        
        this.mode = initDevice.device;
        this.init();
    }
    init() {
        let item = this.DOM.el;
        gsap.set(item, {
            y: '280px',
        });
        gsap.to(item, {
            scrollTrigger: {
                trigger: item,
                start: "top 98%",
                end: "top 2%",
                toggleActions: "restart pause reverse pause",
                scrub: true,
            },
            y: 0
        });
    }
}

class AppSingle {
    constructor () {
        this.el = document.querySelector('.random');
        this.header = document.querySelector('.header');
        this.cursor = document.querySelector('.cursor');
        this.works = [];
        this.items = [...this.el.querySelectorAll('.random__featured-image')];
        
        this.mode = initDevice.device;

        this.setRandomStyle();

        this.init();
    }
    setRandomStyle() {
        let randomStyle = utils.getRandomNumber( 1, 2 );
        if( randomStyle == 2 ) this.el.classList.add("is-randomed");
    }
    init() {
        if( this.mode == 'desktop' ){
            const smoothScrol = new SmoothScroll();
            window.addEventListener('mousemove', (e) => {
                gsap.to(this.cursor,{x:e.clientX,y:e.clientY, opacity: 1});
            });
            gsap.to(document.querySelector('.scroll-icon'), {opacity:1, duration: .3, delay: 2.4})
            gsap.to(document.querySelector('.scroll-icon'), {opacity:0, duration: .3, delay: 6})
            this.enter_animate();
            if(this.items.length > 2){
                this.items.splice(0, 2);
                this.items.forEach(item => new Work(item));
                
                ScrollTrigger.batch(this.items, {
                    onEnter: elements => {
                        gsap.to(elements, {opacity: 1});
                    }
                });
            }
        }else{
            let items_animation = [...this.items.splice(0, 2), document.querySelector('.random__featured-content')];
            gsap.set(items_animation, {y:100, opacity:0});
            gsap.to(items_animation, {
                y: 0,
                opacity: 1,
                stagger: {
                    each: 0.5
                },
                duration: 1
            })
        }
        ScrollTrigger.create({
            trigger: ".page-wrapper",
            start: "top top",
            onUpdate: self => {
                if (self.direction == 1) {
                    this.header.classList.add('header--down');
                }else{
                    this.header.classList.remove('header--down');
                }
                gsap.to(document.querySelector('.scroll-icon'), {opacity:0, duration: .3})
            }
        });
    }
    enter_animate() {
        let animation_elements = this.el.querySelectorAll('.random__featured-image--first-load');
        let content = this.el.querySelector('.random__featured-content');
        gsap.to(animation_elements, {
            y: 0,
            ease: CustomEase.create("custom", "M0,0 C0.054,0.978 0.04,1 1,1 "),
            stagger: {
                each: 0.3
            },
            duration: 2,
            delay: 1.7
        });
        gsap.set(content, {
            y: '50vh',
            opacity: 0
        });
        gsap.to(content, {
            scrollTrigger: {
                trigger: this.el,
                start: "top top",
                once: true
            },
            y: 0,
            opacity: 1,
            duration: 1.3
        });
    }
}

//Init
const initDevice = new InitDevice();
const appSingle = new AppSingle();