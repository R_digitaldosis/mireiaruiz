import { utils } from './modul-utils.js';
import { initDevice } from './modul-initDevice.js';
import { Grid } from './modul-grid.js';

class AppHome {
    constructor( grid, device, screenSize ) {
        this.mode = device;
        this.grid = grid;
        this.size = screenSize;
        
        this.init();
        this.animate();
    }
    init() {
        if( this.mode === 'desktop' ) {
            this.grid.gridItems.forEach((item) => {
                item.DOM.el.addEventListener('mouseenter', () => item.enter());
                item.DOM.el.addEventListener('mouseleave', () => item.leave());
            });
            this.grid.move();
        } else {
            let self = this;
            const container = document.querySelector('.outter-grid');
            const elem = document.querySelector('.grid');
            
            interact(elem)
            .draggable({
                inertia: true,
                modifiers: [
                interact.modifiers.restrictRect({
                    restriction: container,
                    endOnly: true
                })
                ],
                autoScroll: true,
                listeners: {
                move: dragMoveListener,
                }
            })
    
            function dragMoveListener (event) {
                var target = event.target
                var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
                var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy
        
                gsap.to(elem, {
                    x: x,
                    y: y,
                    duration: 1
                });
        
                target.setAttribute('data-x', x)
                target.setAttribute('data-y', y)
            }
        }
    }
    animate() {
        const cols = document.querySelectorAll('.grid__column');
        //const colsTop = document.querySelector('.grid__column--top');
        //const colsBottom = document.querySelector('.grid__column--bottom');
        gsap.to(cols, {
            y: 0,
            duration: 5,
            //ease: Quart.easeInOut,
            ease: CustomEase.create("custom", "M0,0 C0.054,0.978 0.04,1 1,1 "),
            stagger: {
                each: .1,
            }
        });
    }
}

//Init Home
//Load
const grid = new Grid(document.querySelector('.grid'));
//Init
const app = new AppHome(grid, initDevice.device, initDevice.size);