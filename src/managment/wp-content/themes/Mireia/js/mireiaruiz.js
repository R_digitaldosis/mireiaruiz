
import { utils } from './modul-utils.js';
import { mainMenu } from './modul-menu.js';
import { checkOrientation } from './modul-initDevice.js';

imagesLoaded(document.querySelectorAll('img'), () => {

    //Loading...
    checkOrientation();
    document.getElementsByClassName('loader')[0].classList.remove('loading');

    document.querySelector('.header__logo a').addEventListener('click', function(e){
        e.preventDefault();
        let target = e.target.parentNode.getAttribute('href');
        gsap.to('body', {opacity: 0, duration: .2, onComplete: function() {
            window.location.href = target
        }})
    })
});

