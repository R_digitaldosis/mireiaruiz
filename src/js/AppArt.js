import { utils } from './modul-utils.js';
import { initDevice } from './modul-initDevice.js'
class ItemWorks {
    constructor(el) {
        this.DOM = {el: el};
        this.maskLat = this.DOM.el.querySelectorAll('.slider__item-mask--laterals');
        this.maskVert = this.DOM.el.querySelectorAll('.slider__item-mask--verticals');
        /* this.maskLeft = this.DOM.el.querySelector('.slider__item-mask--left');
        this.maskRight = this.DOM.el.querySelector('.slider__item-mask--right'); */
    }
    enter() {
        gsap.to( this.maskLat, {width:0, duration:1});
        gsap.to( this.maskVert, {height:0, duration:1});
    }
    leave() {
        gsap.to( this.maskLat, {width:16, duration:1});
        gsap.to( this.maskVert, {height:16, duration:1});
    }
    click() {
        let source_link = this.src;
        console.log(source_link);
        //this.lastScale = .4;
    }
}

class AppArt {
    constructor () {
        this.el = document.querySelector('.slider');
        this.container = document.querySelector('.page-wrapper');
        this.works = [];
        this.items = [...this.el.querySelectorAll('.slider__item')];
        this.items.forEach(item => this.works.push(new ItemWorks(item)));
        this.mode = initDevice.device;

        this.init();
        //this.move();
    }
    init() {
        if( this.mode === 'desktop' ) {
            this.works.forEach((item) => {
                item.DOM.el.addEventListener('mouseenter', () => item.enter());
                item.DOM.el.addEventListener('mouseleave', () => item.leave());
            });
            this.move();
        } else {
            //const appHomeMobile = new AppMobile(this.size);
            this.grab();
        }
    }
    grab() {
        let self = this;
        const container = this.container;
        const elem = this.el;
        const position = { x: 0, y: 0 }
        
        interact(elem)
        .draggable({
            inertia: true,
            startAxis: 'x',
            lockAxis: 'x',
            modifiers: [
                interact.modifiers.restrictRect({
                    restriction: container,
                    endOnly: true
                })
            ],
            listeners: {
                move (event) {
                    var target = event.target
                    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
            
                    gsap.to(elem, {
                        x: x,
                        duration: 1
                    });
            
                    target.setAttribute('data-x', x)
                },
            }
        })
    }
    move() {
        let translationVal = 0;
        // get random start and end movement boundaries
        /* const xstart = getRandomNumber(15,60);
        const ystart = getRandomNumber(15,60); */
        let sliderWidth = this.el.offsetWidth;
        let containerWidth = this.container.offsetWidth;
        const xstart = (sliderWidth - containerWidth) / 2;
        //console.log(sliderWidth + ' ' + containerWidth + ' ' + xstart);

        const render = () => {
            translationVal = utils.lerp(translationVal, utils.map(mousepos.x, 0, containerWidth, -xstart, xstart), 0.07);

            gsap.set(this.el, {x: -translationVal});   
            requestAnimationFrame(render);
        }
        requestAnimationFrame(render);
    }
}


//Init
let mousepos = {x: initDevice.size.width/2, y: initDevice.size.height/2};
window.addEventListener('mousemove', ev => mousepos = utils.getMousePos(ev));
const appArt = new AppArt();