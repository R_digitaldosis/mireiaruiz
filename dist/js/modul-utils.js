export const calcWinsize = () => {
    let winSize = {
        width: window.innerWidth || document.documentElement.clientWidth || 
        document.body.clientWidth,
        height: window.innerHeight|| document.documentElement.clientHeight|| 
        document.body.clientHeight
    }
    return {width: winSize.width, height: winSize.height};
};

// Map number x from range [a, b] to [c, d]
const map = (x, a, b, c, d) => (x - a) * (d - c) / (b - a) + c;

// Linear interpolation
const lerp = (a, b, n) => (1 - n) * a + n * b;


const getRandomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

// Gets the mouse position
const getMousePos = (e) => {
    let posx = 0;
    let posy = 0;
    if (!e) e = window.event;
    if (e.pageX || e.pageY) {
        posx = e.pageX;
        posy = e.pageY;
    }
    else if (e.clientX || e.clientY)    {
        posx = e.clientX + body.scrollLeft + document.documentElement.scrollLeft;
        posy = e.clientY + body.scrollTop + document.documentElement.scrollTop;
    }
    
    return { x : posx, y : posy }
};

export const utils = {
    calcWinsize,
    map,
    lerp,
    getRandomNumber,
    getMousePos
}