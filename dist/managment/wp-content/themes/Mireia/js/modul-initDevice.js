import { calcWinsize } from './modul-utils.js'
// Get Device
const mediaQuery = function(query) {
    var breakpoints = {
    mobile: '(min-width: 320px)',
    tablet: '(min-width: 768px)',
    desktop: '(min-width: 1024px)',
    retina:
        'only screen and (-webkit-min-device-pixel-ratio:2), only screen and (min--moz-device-pixel-ratio:2), only screen and (min-device-pixel-ratio:2), only screen and (min-resolution:2dppx), only screen and (min-resolution:192dpi)',
    };
    switch (query) {
    case 'mobile':
        return window.matchMedia(breakpoints.mobile).matches && !window.matchMedia(breakpoints.tablet).matches;
    case 'tablet':
        return window.matchMedia(breakpoints.tablet).matches && !window.matchMedia(breakpoints.desktop).matches;
    case 'desktop':
        return window.matchMedia(breakpoints.desktop).matches;
    case 'retina':
        return window.matchMedia(breakpoints.retina).matches;
    default:
        return false;
    }
};
// Get Device
export const getCurrentBreakpoint = function() {
    var current = mediaQuery('mobile') ? 'mobile' : null;
    current = mediaQuery('tablet') ? 'tablet' : current;
    current = mediaQuery('desktop') ? 'desktop' : current;
    return current;
};

// Init Device
export class InitDevice {
    constructor() {
        this.device;
        this.size = calcWinsize();

        this.setDevice();

        window.addEventListener("resize", () => this.reloadDevice());
    }
    getSize() {
        return this.size;
    }
    //init() {
    setDevice() {
        this.device = getCurrentBreakpoint() !== 'desktop' ? 'mobile' : 'desktop';
    }
    getDevice() {
        return this.device;
    }
    reloadDevice() {
        let lastDevice = this.device;
        this.setDevice();
        if (lastDevice !== this.device) location.reload();
    }
}
export class LargeDevice extends InitDevice {
    constructor() {
        super();

        this.subSize;
        this.setSubSize();
    }
    setSubSize() {
        let actualW = calcWinsize();
        if ( this.device == 'desktop' ) {
            this.subSize = actualW.width >= 1600 ? 'large' : 'regular';
        }
    }
    reloadDevice() {
        super.reloadDevice();

        let lastSubSize = this.subSize;
        this.setSubSize();
        if (lastSubSize !== this.subSize) location.reload();
    }
}
export const checkOrientation = function (){
    var currMode = "";

    switch(window.orientation){

         case 0:
         currMode = "portrait";
         break;

         case -90:
         currMode = "landscape";
         break;

         case 90:
         currMode = "landscape";
         break;

         case 180:
         currMode = "landscape";
         break;
   }
    document.getElementsByTagName("body")[0].setAttribute("class", currMode);
}