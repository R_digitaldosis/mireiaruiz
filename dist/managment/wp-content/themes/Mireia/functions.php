<?php

function mireia_setup() {

	load_theme_textdomain( 'mireia' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );

	// Menus
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'mireia' ),
		'footer'  => __( 'Footer Menu', 'mireia' ),
		//'legal'  => __( 'Legal Menu', 'mireia' )
	) );

	function add_additional_class_on_li($classes, $item, $args) {
		if(isset($args->add_li_class)) {
			$classes[] = $args->add_li_class;
		}
		return $classes;
	}
	add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio'
	) );

	add_image_size( 'thumb-home-sm', 733, 1141, false ); // Home mobile
	add_image_size( 'thumb-works-sm', 815, 1223, false ); // Works mobile
	add_image_size( 'thumb-lg', 600, 900, false ); // Home & Works Desktop
	add_image_size( 'thumb-work-sm', 964, 1446, false ); // Single mobile
    add_image_size( 'thumb-work-lg', 770, 1156, false ); // Single desktop

	/*$color_scheme  = mireia_get_color_scheme();
	$default_color = trim( $color_scheme[0], '#' );*/
}

add_action( 'after_setup_theme', 'mireia_setup' );

/** Widget area */
function mireia_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Contact Area', 'mireia' ),
		'id'            => 'header-contact',
		'description'   => __( 'Heder contact area', 'mireia' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'mireia_widgets_init' );


function mireia_scripts() {

	wp_enqueue_style( 'mireia-style', get_stylesheet_uri() );

  	//wp_enqueue_style( 'mireia-vendors', get_stylesheet_directory_uri() . '/css/vendors.min.css' );
	wp_enqueue_style( 'mireia-main', get_template_directory_uri() . '/css/main.min.css');
	
	wp_enqueue_script( 'mireia-vendors', get_template_directory_uri() . '/js/vendors.min.js', array('jquery'), null, true);
	wp_enqueue_script( 'mireia-scripts', get_template_directory_uri() . '/js/mireiaruiz.js', array('mireia-vendors'), '1.0.0', true );

	//wp_localize_script( 'mireia-script', 'ajax_url', admin_url('admin-ajax.php') );
}
add_action( 'wp_enqueue_scripts', 'mireia_scripts' );

function my_enqueue_stuff() {
	if ( is_page_template( 'templates/home.php' ) ) {
		wp_enqueue_script( 'mireia-home', get_template_directory_uri() . '/js/AppHome.js', array('mireia-vendors'), '1.0.0', true );
	} elseif( is_page_template( 'templates/works.php' ) ){
		wp_enqueue_script( 'mireia-work', get_template_directory_uri() . '/js/AppArt.js', array('mireia-vendors'), '1.0.0', true );
	} elseif( is_page_template( 'templates/about.php' ) ) {
		wp_enqueue_script( 'mireia-single', get_template_directory_uri() . '/js/AppAbout.js', array('mireia-vendors'), '1.0.0', true );
	} elseif( is_singular( 'works' )) {
		wp_enqueue_script( 'mireia-single', get_template_directory_uri() . '/js/AppSingle.js', array('mireia-vendors'), '1.0.0', true );
	}
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_stuff' );

add_action( 'login_enqueue_scripts', 'my_custom_login_enqueue_scripts' );

function my_custom_login_enqueue_scripts() {
    wp_enqueue_style( 'my-custom-login', get_stylesheet_directory_uri() . '/css/login-style.css' );
}

add_filter('script_loader_tag', 'add_type_attribute' , 10, 3);

function add_type_attribute($tag, $handle, $src) {
	if ( ('mireia-scripts' === $handle) || ('mireia-home' === $handle) || ('mireia-work' === $handle) || ('mireia-single' === $handle)){
		//$src = '/wp-content/plugins/my-plugin/js/module1.js';
		$tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
	}
	return $tag;
}

// Favicon
function add_favicon() {
	$favicon_url = get_stylesheet_directory_uri() . '/img/favicon.png';
  echo '<link rel="shortcut icon" type="image/png" href="' . $favicon_url . '" />';
}

add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');

//hide admin bar
show_admin_bar(false);

function custom_posts_init() {

	$args_cpost = array(
		'labels' => array(
						'name' => __( 'Works', 'mireia' ),
						'singular_name' => __( 'Work', 'mireia' )
			),
			'public' => true,
			//'rewrite' => array( 'slug' => __( '', 'mireia' ) ),
			//'has_archive' => 'events',
			'menu_position' => null,
			//'menu_icon' => 'dashicons-calendar-alt',
			'supports'           => array( 'title', 'thumbnail', 'excerpt' ),
			'taxonomies' => array( 'type')//'post_tag',
	);

	register_post_type( 'works', $args_cpost );
}

add_action( 'init', 'custom_posts_init' );

function create_taxonomy() {

	$args_event_tax = array(
	'labels' => array( 'name' => _x( 'Type', 'taxonomy general name', 'mireia' ) ),
	//'rewrite' => array( 'slug' => 'area' ),
	'hierarchical' => true,
	'query_var'    => true,
	'show_admin_column' => true
	);

	register_taxonomy( 'Type', array('works') , $args_event_tax );
}

add_action( 'init', 'create_taxonomy' );



//remove unnecessary default files
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
function wpassist_remove_innecesary(){
	wp_dequeue_style( 'wp-block-library' );
} 
add_action( 'wp_enqueue_scripts', 'wpassist_remove_innecesary' );
function dequeue_jquery_migrate($scripts){
    if(!is_admin() && !empty($scripts->registered['jquery'])){
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps=array_diff($jquery_dependencies,array('jquery-migrate'));
    }
}
add_action( 'wp_default_scripts', 'dequeue_jquery_migrate' );
function my_deregister_scripts(){
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);

//custom client-logo login page
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/Logo-MR.svg);
			height:auto;
			width: 147px;
			background-size: 147px 13px;
			background-repeat: no-repeat;
        	padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
