
class Menu {
    constructor() {
        this.container = document.querySelector('.header__menu');
        this.el = document.querySelector('.header__menu .menu');
        this.btn = document.querySelector('.header__icon-menu-mobile');
        this.item_contact = this.el.querySelector('.menu__item--contact');
        this.contact = document.querySelector('.contact');
        this.elements_animation = [...this.el.querySelectorAll('li'), ...this.contact.querySelectorAll('li')];
        this.elements_exit_transition = [...this.el.querySelectorAll('.exit-link')];
        this.animation = gsap.timeline({delay:.3});

        this.btn.addEventListener("click", () => {
            this.container.classList.toggle('is-open');
            this.btn.classList.toggle('is-active');
            document.querySelector('body').classList.toggle('non-scroll');
            this.animation.restart(true);
        });

        this.item_contact.addEventListener("click", (e) => {
            e.preventDefault();
            const contact_width = this.contact.offsetWidth;
            this.item_contact.classList.toggle('active');
            this.contact.classList.toggle('is-visible');
            let Xtranslate = this.contact.classList.contains('is-visible') ? -(contact_width) : 0;
            gsap.to(this.el, {
                x: Xtranslate,
                duration: .4
            });
        });

        this.elements_exit_transition.forEach( (menuitem) => {
            menuitem.addEventListener("click", (e) => {
                e.preventDefault();
                let target = e.target.getAttribute('href');
                gsap.to('body', {opacity: 0, duration: .2, onComplete: function() {
                    window.location.href = target
                }})
            });
        });

        this.show();
        this.animate();
    }

    show() {
        const btn_actions = [document.querySelector('.header__logo'), document.querySelector('.hamburger'),this.el, document.querySelector('.footer__credits')];
        gsap.to(btn_actions, {
            y: 0,
            opacity: 1,
            duration: .8,
            delay: 1
        });
    }

    animate() {
        this.animation.to(this.elements_animation, {
            x: 0,
            opacity: 1,
            stagger: 0.1,
            duration: .3
        });
    }
}

export const mainMenu = new Menu();