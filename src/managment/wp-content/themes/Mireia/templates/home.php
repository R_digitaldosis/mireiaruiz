<?php
/*
Template Name: Home
*/

get_header(); 
?>
<div class="loader loading"></div>
<div class="page-wrapper page-wrapper--fixed">
    <div class="outter-grid">
        <div class="grid">
            <div class="grid__column grid__column--bottom is-desktop">
                <?php while( have_rows('column_1') ): the_row();
                    $featured_post_id = get_sub_field('item_column');
                    $permalink = get_permalink( $featured_post_id );
                    $image = get_the_post_thumbnail_url( $featured_post_id, 'thumb-lg' );
                    $image_webp = $image.'.webp';
                    $title = get_the_title( $featured_post_id);
                ?>
                <div class="grid__item">
                    <a href="<?=$permalink?>" aria-label="<?=$title?>"><picture>
                        <source srcset="<?=$image_webp?>" type="image/webp">
                        <source srcset="<?=$image?>" type="image/jpeg">
                        <img src="<?=$image?>" alt="<?=$title?>" width="600" height="900">
                    </picture></a>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--top"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--left"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--right"></div>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--bottom"></div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="grid__column grid__column--top grid__column--center">
                <?php while( have_rows('column_2') ): the_row();
                    $featured_post_id = get_sub_field('item_column');
                    $permalink = get_permalink( $featured_post_id );
                    $image = get_the_post_thumbnail_url( $featured_post_id, 'thumb-lg' );
                    $image_webp = $image.'.webp';
                    $title = get_the_title( $featured_post_id);
                ?>
                <div class="grid__item">
                    <a href="<?=$permalink?>" aria-label="<?=$title?>"><picture>
                        <source srcset="<?=$image_webp?>" type="image/webp">
                        <source srcset="<?=$image?>" type="image/jpeg">
                        <img src="<?=$image?>" alt="<?=$title?>" width="600" height="900">
                    </picture></a>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--top"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--left"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--right"></div>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--bottom"></div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="grid__column grid__column--bottom grid__column--center">
                <?php while( have_rows('column_3') ): the_row();
                    $featured_post_id = get_sub_field('item_column');
                    $permalink = get_permalink( $featured_post_id );
                    $image = get_the_post_thumbnail_url( $featured_post_id, 'thumb-lg' );
                    $image_webp = $image.'.webp';
                    $title = get_the_title( $featured_post_id);
                ?>
                <div class="grid__item">
                    <a href="<?=$permalink?>" aria-label="<?=$title?>"><picture>
                        <source srcset="<?=$image_webp?>" type="image/webp">
                        <source srcset="<?=$image?>" type="image/jpeg">
                        <img src="<?=$image?>" alt="<?=$title?>" width="600" height="900">
                    </picture></a>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--top"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--left"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--right"></div>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--bottom"></div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="grid__column grid__column--top grid__column--center">
                <?php while( have_rows('column_4') ): the_row();
                    $featured_post_id = get_sub_field('item_column');
                    $permalink = get_permalink( $featured_post_id );
                    $image = get_the_post_thumbnail_url( $featured_post_id, 'thumb-lg' );
                    $image_webp = $image.'.webp';
                    $title = get_the_title( $featured_post_id);
                ?>
                <div class="grid__item">
                    <a href="<?=$permalink?>" aria-label="<?=$title?>"><picture>
                        <source srcset="<?=$image_webp?>" type="image/webp">
                        <source srcset="<?=$image?>" type="image/jpeg">
                        <img src="<?=$image?>" alt="<?=$title?>" width="600" height="900">
                    </picture></a>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--top"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--left"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--right"></div>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--bottom"></div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="grid__column grid__column--bottom is-desktop">
                <?php while( have_rows('column_5') ): the_row();
                    $featured_post_id = get_sub_field('item_column');
                    $permalink = get_permalink( $featured_post_id );
                    $image = get_the_post_thumbnail_url( $featured_post_id, 'thumb-lg' );
                    $image_webp = $image.'.webp';
                    $title = get_the_title( $featured_post_id);
                ?>
                <div class="grid__item">
                    <a href="<?=$permalink?>" aria-label="<?=$title?>"><picture>
                        <source srcset="<?=$image_webp?>" type="image/webp">
                        <source srcset="<?=$image?>" type="image/jpeg">
                        <img src="<?=$image?>" alt="<?=$title?>" width="600" height="900">
                    </picture></a>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--top"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--left"></div>
                    <div class="grid__item-mask grid__item-mask--laterals grid__item-mask--right"></div>
                    <div class="grid__item-mask grid__item-mask--verticals grid__item-mask--bottom"></div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
    <svg class="cursor cursor--circle" width="80" height="80" viewBox="0 0 80 80">
        <circle id="circle-inner" class="cursor--circle__inner" cx="40" cy="40" r="20"/>
    </svg>
    <svg class="cursor cursor--pointer" width="6" height="6" viewBox="0 0 6 6">
        <circle class="cursor--pointer__point" cx="3" cy="3" r="3"/>
    </svg>
</div>
<?php
get_footer();
?>