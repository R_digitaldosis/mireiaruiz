	<footer class="footer <?php if( is_single() || is_page_template('templates/about.php')) : echo ' footer--bottom'; endif?>" id="footer">
	  <div class="footer__credits credits link"><a href="https://digitaldosis.com" target="_blank" rel="noreferrer">Site by DigitalDosis</a></div>
	  <div class="footer__copyright is-mobile">© Mireia Ruiz</div>
  	</footer>
<?php wp_footer(); ?>
<div class="advise-orientation"></div>
</body>
</html>
