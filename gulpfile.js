'use strict';
// global requires
var gulp = require('gulp'),

    del = require('del'),
    concat = require('gulp-concat'),
    svg = require('gulp-svg-sprite'),
    rename = require('gulp-rename'),
    plumber = require('gulp-plumber'),
    cssnano = require('gulp-cssnano'),
    browserSync = require('browser-sync').create(),
    sass = require('gulp-sass');



var config = require('./config.json'),
//path = 'dist/';
path = 'dist/managment/wp-content/themes/Mireia/';

// cleanup /dist folder
gulp.task('clean', function() {
    return del.sync(path);
});

gulp.task('css-vendors', function() {
    return gulp.src([
            // 'node_modules/flickity/dist/flickity.min.css',
            // 'node_modules/air-datepicker/dist/css/datepicker.min.css'
        ])
        .pipe(browserSync.stream())
        .pipe(concat('vendors.min.css'))
        .pipe(gulp.dest(path+'/css'));
});

// moves all html from /src to /dist
gulp.task('markup', function() {
    // return gulp.src(['src/**/*.json', 'src/**/*.ico'])
    return gulp.src([
        'src/managment/wp-content/themes/Mireia/**/*.json',
        'src/managment/wp-content/themes/Mireia/**/*.css',
        'src/managment/wp-content/themes/Mireia/**/*.js',
        'src/managment/wp-content/themes/Mireia/**/*.ico',
        'src/managment/wp-content/themes/Mireia/**/*.php',
        'src/managment/wp-content/themes/Mireia/**/*.html',
        'src/managment/wp-content/themes/Mireia/**/*.twig',
        'src/managment/wp-content/themes/Mireia/**/*.png',
        'src/managment/wp-content/themes/Mireia/**/*.jpg',
        'src/managment/wp-content/themes/Mireia/**/*.gif',
        'src/managment/wp-content/themes/Mireia/**/*.svg',
        'src/managment/wp-content/themes/Mireia/**/*.mp4',
        'src/managment/wp-content/themes/Mireia/**/*.webm'
    ])
    .pipe(browserSync.stream())
    .pipe(gulp.dest(path+'/'));
});
// moves all fonts from vendors/src to /dist
gulp.task('fonts', function () {
	return gulp.src(['src/fonts/**/*.*'])
		.pipe(browserSync.stream())
		.pipe(gulp.dest(path+'/fonts'));
});

// moves all images from /src to /dist
gulp.task('images', function() {
    return gulp.src(['src/img/*.*', 'src/img/*/*.*'])
        .pipe(browserSync.stream())
        .pipe(gulp.dest(path+'/img'));
});

// vendors
gulp.task('vendors', function() {
    return gulp.src([
            'node_modules/gsap/dist/gsap.min.js',
            'src/js/vendors/gsap/CustomEase.min.js',
            'node_modules/imagesloaded/imagesloaded.pkgd.min.js',
            'node_modules/interactjs/dist/interact.min.js',
            'node_modules/gsap/dist/ScrollTrigger.min.js',
        ])
        .pipe(plumber())
        .pipe(concat('vendors.min.js'))
        .pipe(gulp.dest(path+'/js'));
});

// scripts
gulp.task('scripts', function() {
    return gulp.src(['src/**/*.js'])

    .pipe(plumber())
    .pipe(concat('app.min.js'))
    .pipe(browserSync.stream())
    .pipe(gulp.dest(path+'/js'));
});

console.log('PORT-----------', config.port);
gulp.task('browser-sync', function() {

    return browserSync.init({

        proxy: config.proxy,
        host: config.proxy,
        open: 'external',
    });

});

//Compile scss files
gulp.task('sass', function() {
    var postcss = require('gulp-postcss');
    var autoprefixer = require('autoprefixer');

    var processors = [autoprefixer({
        browsers: ['last 2 versions', 'ie >= 10'],
        cascade: false,
        grid: true
    })];
    gulp.src('scss/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
        }).on('error', sass.logError))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cssnano({
            zindex: false,
            reduceTransforms: false
        }))
        .pipe(postcss(processors))
        .pipe(browserSync.stream())
        .pipe(gulp.dest(path+'/css'));

});

// watch files for live reload
gulp.task('watch', function() {
    // gulp.watch('src/css/**/*.css', ['css']);
    gulp.watch('src/managment/wp-content/themes/Mireia/js/**/*.js', ['markup']);
    gulp.watch(['src/**/*.json', 'src/**/.htaccess'], ['markup']);
    gulp.watch('scss/**/*.scss', ['sass']);

    gulp.watch([
        'src/managment/wp-content/themes/Mireia/**/*.html',
        'src/managment/wp-content/themes/Mireia/**/*.php',
        'src/**/*.twig'
    ], ['markup']).on('change', browserSync.reload);
});

// tasks
gulp.task('build', ['clean', 'vendors', 'css-vendors', 'markup', 'fonts', 'images', 'scripts', 'sass']);
gulp.task('rocks', ['build', 'browser-sync', 'watch']);
