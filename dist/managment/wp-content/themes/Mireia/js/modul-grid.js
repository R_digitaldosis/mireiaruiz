import { utils } from './modul-utils.js';

class ItemGrid {
    constructor(el) {
        this.DOM = {el: el};
        this.mask = this.DOM.el.querySelector('.grid__item-mask');
        this.maskLat = this.DOM.el.querySelectorAll('.grid__item-mask--laterals');
        this.maskVert = this.DOM.el.querySelectorAll('.grid__item-mask--verticals');

        this.DOM.el.addEventListener('click', function(e){
            e.preventDefault();
            let target = e.target.parentNode.parentNode.getAttribute('href');
            gsap.to('body', {opacity: 0, duration: .2, onComplete: function() {
                window.location.href = target
            }})
        })
    }
    enter() {
        gsap.to( this.maskLat, {width:0, duration:1});
        gsap.to( this.maskVert, {height:0, duration:1});
    }
    leave() {
        gsap.to( this.maskLat, {width:35, duration:1});
        gsap.to( this.maskVert, {height:35, duration:1});
    }
    click() {
        //this.lastScale = .4;
    }
}

export class Grid {
    constructor(el) {
        this.DOM = {el: el};
        this.gridItems = [];
        this.items = [...document.querySelectorAll('.grid__item')];
        this.items.forEach(item => this.gridItems.push(new ItemGrid(item)));
        this.winsize = utils.calcWinsize();

        //this.showItems();
    }
    showItems(){
        /* Enter Animation */
    }
    move (){
        let translationVals = {tx: 0, ty: 0};
        let gridWidth = this.DOM.el.offsetWidth;
        let gridHeight = this.DOM.el.offsetHeight;
        const xstart = (gridWidth - this.winsize.width) / 2;
        const ystart = (gridHeight - this.winsize.height) / 2;

        // infinite loop
        const render = () => {
            // Calculate the amount to move.
            // Using linear interpolation to smooth things out. 
            // Translation values will be in the range of [-start, start] for a cursor movement from 0 to the window's width/height
            translationVals.tx = utils.lerp(translationVals.tx, utils.map(mousepos.x, 0, this.winsize.width, -xstart, xstart), 0.07);
            translationVals.ty = utils.lerp(translationVals.ty, utils.map(mousepos.y, 0, this.winsize.height, -ystart, ystart), 0.07);
            
            gsap.set(this.DOM.el, {x: -translationVals.tx, y: -translationVals.ty});   
            requestAnimationFrame(render);
        }
        requestAnimationFrame(render);
    }
}

let mousepos = {x: utils.calcWinsize().width/2, y: utils.calcWinsize().height/2};
window.addEventListener('mousemove', ev => mousepos = utils.getMousePos(ev));