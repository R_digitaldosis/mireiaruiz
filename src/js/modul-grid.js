import { utils } from './modul-utils.js';

class ItemGrid {
    constructor(el) {
        this.DOM = {el: el};
        this.mask = this.DOM.el.querySelector('.grid__item-mask');
    }
    enter() {
        gsap.to( this.mask, {border:0, duration:1});
    }
    leave() {
        gsap.to( this.mask, {border:35, duration:1});
    }
    click() {
        //this.lastScale = .4;
    }
}

export class Grid {
    constructor(el) {
        this.DOM = {el: el};
        this.gridItems = [];
        this.items = [...this.DOM.el.querySelectorAll('.grid__item')];
        this.items.forEach(item => this.gridItems.push(new ItemGrid(item)));
        this.winsize = utils.calcWinsize();

        this.showItems();
        //this.move();
    }
    showItems(){
        /* Enter Animation */
    }
    move (){
        let translationVals = {tx: 0, ty: 0};
        // get random start and end movement boundaries
        /* const xstart = getRandomNumber(15,60);
        const ystart = getRandomNumber(15,60); */
        let gridWidth = this.DOM.el.offsetWidth;
        let gridHeight = this.DOM.el.offsetHeight;
        const xstart = (gridWidth - this.winsize.width) / 2;
        const ystart = (gridHeight - this.winsize.height) / 2;

        // infinite loop
        const render = () => {
            // Calculate the amount to move.
            // Using linear interpolation to smooth things out. 
            // Translation values will be in the range of [-start, start] for a cursor movement from 0 to the window's width/height
            translationVals.tx = utils.lerp(translationVals.tx, utils.map(mousepos.x, 0, this.winsize.width, -xstart, xstart), 0.07);
            translationVals.ty = utils.lerp(translationVals.ty, utils.map(mousepos.y, 0, this.winsize.height, -ystart, ystart), 0.07);
            
            gsap.set(this.DOM.el, {x: -translationVals.tx, y: -translationVals.ty});   
            requestAnimationFrame(render);
        }
        requestAnimationFrame(render);
    }
}

let mousepos = {x: utils.calcWinsize().width/2, y: utils.calcWinsize().height/2};
window.addEventListener('mousemove', ev => mousepos = utils.getMousePos(ev));