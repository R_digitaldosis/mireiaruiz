<?php
/*
Template Name: Works
*/

get_header(); 
global $post;
    $post_slug = $post->post_name;
    $args = array(
        'post_type' => 'works',
        'posts_per_page' => '-1',
        'order' => 'ASC',
        'tax_query' => array(
            array(
                'taxonomy' => 'Type',
                'field' => 'slug',
                'terms' => array( $post_slug ),
                'operator' => 'AND'
            )
        )
    );
    $the_query = new WP_Query( $args );
?>
<div class="loader loading"></div>
<div class="page-wrapper page-wrapper--projects">
    <div class="slider">
    <?php
    $f = 0;
    if ( $the_query->have_posts() ) :
        while ( $the_query->have_posts() ) : $the_query->the_post(); 
            $class_name = ( $f%2==0 ) ? "slider__item--top" : "slider__item--bottom"; ?>
            <div class="slider__item <?= $class_name;?>">
                <div class="slider__item-mask slider__item-mask--verticals slider__item-mask--top"></div>
                <div class="slider__item-mask slider__item-mask--laterals slider__item-mask--left"></div>
                <?php
                if ( has_post_thumbnail() ) : ?>
                    <a href="<?=get_permalink()?>">
                    <picture>
                        <source
                            srcset="<?php the_post_thumbnail_url( 'thumb-lg' );echo '.webp';?>"
                            type="image/webp">
                        <img
                            src="<?php the_post_thumbnail_url( 'thumb-lg' );?>" alt="<?php the_title();?>">
                    </picture>
                    </a>
                <?php
                endif;
                ?>
                <div class="slider__item-mask slider__item-mask--laterals slider__item-mask--right"></div>
                <div class="slider__item-mask slider__item-mask--verticals slider__item-mask--bottom"></div>
            </div>
        <?php
        $f++;
        endwhile;
    endif;
    // Reset Post Data
    wp_reset_postdata();
    ?>
    </div>
    <svg class="cursor cursor--circle" width="80" height="80" viewBox="0 0 80 80">
        <circle id="circle-inner" class="cursor--circle__inner" cx="40" cy="40" r="20"/>
    </svg>
    <svg class="cursor cursor--pointer" width="34.5" height="10" viewBox="0 0 23.77 10">
        <g>
            <path d="M5.47,0.19c-0.26-0.26-0.67-0.26-0.93,0L0.19,4.53c-0.26,0.26-0.26,0.67,0,0.93l4.34,4.34c0.26,0.26,0.67,0.26,0.93,0
                c0.26-0.26,0.26-0.67,0-0.93L1.59,5l3.88-3.88C5.72,0.87,5.72,0.45,5.47,0.19z"/>
            <path d="M23.57,4.53l-4.34-4.34c-0.26-0.26-0.67-0.26-0.93,0c-0.26,0.26-0.26,0.67,0,0.93L22.18,5L18.3,8.88
                c-0.26,0.26-0.26,0.67,0,0.93c0.26,0.26,0.67,0.26,0.93,0l4.34-4.34C23.83,5.21,23.83,4.79,23.57,4.53z"/>
        </g>
        <circle class="cursor--pointer__point cursor--pointer__point--works" cx="3" cy="3" r="3"/>
    </svg>
</div>
<?php
get_footer();
?>