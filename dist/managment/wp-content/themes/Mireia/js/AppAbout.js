import { InitDevice } from './modul-initDevice.js';
import SmoothScroll from './modul-scroll.js';

class AppAbout {
    constructor (el) {
        this.DOM = {el: el};
        this.columnLeft = this.DOM.el.querySelector('.column--left');
        this.columnCenter = this.DOM.el.querySelector('.column--center');
        this.columnRight = this.DOM.el.querySelector('.column--last');
        this.header = document.querySelector('.header');
        this.mode = initDevice.device;
        this.cursor = document.querySelector('.cursor');

        this.init();
    }
    init() {
        const top_elements = [...document.querySelectorAll('.column__top')];
        gsap.to( top_elements, { 
            y: 0,
            duration: 2,
            ease: CustomEase.create("custom", "M0,0 C0.054,0.978 0.04,1 1,1 "),
            stagger: {
                each: 0.3
            },
            delay: 1.7
        });

        if( this.mode === 'desktop' ){
            const smoothScroll = new SmoothScroll();
            window.addEventListener('mousemove', (e) => {
                gsap.to(this.cursor,{x:e.clientX,y:e.clientY, opacity: 1});
            });

            let item = this.DOM.el;
            let itemLeft = this.columnLeft;
            let itemRight = this.columnRight;
            let itemsCenterRight= [this.columnCenter.querySelector('.column__image'), this.columnRight];
            gsap.to(itemLeft, {
                scrollTrigger: {
                    trigger: item,
                    start: "top top",
                    end: "bottom bottom",
                    toggleActions: "restart pause reverse pause",
                    scrub: true,
                },
                y: '-300',
                duration: 1
            });
            gsap.to(itemsCenterRight, {
                scrollTrigger: {
                    trigger: this.columnCenter.querySelector('.column__image'),
                    start: "top 75%",
                    toggleActions: "restart pause resume pause",
                    toggleClass: {targets: itemsCenterRight, className: "active"},
                },
                ease: CustomEase.create("custom", "M0,0 C0.054,0.978 0.04,1 1,1 "),
                stagger: {
                    each: 0.3
                },
                y: -100,
                duration: 2
            });
        }else{
            ScrollTrigger.create({
                trigger: ".page-wrapper",
                start: "top top",
                onUpdate: self => {
                    if (self.direction == 1) {
                        this.header.classList.add('header--down');
                    }else{
                        this.header.classList.remove('header--down');
                    }
                }
            });
        }
    }
}


//Init
const initDevice = new InitDevice();
const appAbout = new AppAbout(document.querySelector('.columns'));