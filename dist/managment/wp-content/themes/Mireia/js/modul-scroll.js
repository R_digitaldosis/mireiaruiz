
export default class SmoothScroll {
    constructor() {
        this.el = document.querySelector('.scroll-wrap');
        this.velocity = 0.05;
        this.lastY = 0;
        this.y = 0;
        this.scrolled = 0;
        this.scrolling = 0;
        this.requestId = null;
        this.resetInitPosY = false;
        this.direction = null;

        this.init();
    }
    init() {
        this.setContainerHeight();

        //resetScrollOnLoad
        window.pageYOffset = 0;
        document.documentElement.scrollTop = 0;
        document.body.scrollTop = 0;
        
        document.addEventListener('scroll', event => { this.scroll() });
        window.addEventListener("resize", event => { this.setContainerHeight() });
    }
    setContainerHeight() {
        document.body.style.height = this.el.clientHeight + 'px';
    }
    scroll() {

        const move = () => {
            this.scrolled = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
            this.lastY = this.scrolled;
            this.y += (this.scrolled - this.y) * this.velocity;
            this.direction = this.y > this.lastY ? 'up' : 'down';

            if (Math.abs(this.scrolled - this.y) < 0.1) {
                this.y = this.scrolled;
                this.scrolling = 0;
            }
            
            gsap.set(this.el, { 
                y: -this.y 
            });

            this.requestId = this.scrolling > 0 ? requestAnimationFrame(move) : null;
        }

        this.scrolling++;
        if (!this.requestId) {
            this.requestId = requestAnimationFrame(move);
        }
    }
    getScrollDirection() {
        return this.direction;
    }
}
