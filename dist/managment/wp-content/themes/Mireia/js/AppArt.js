import { utils } from './modul-utils.js';
import { LargeDevice } from './modul-initDevice.js';
import Cursor from './modul-cursor.js';

class ItemWorks {
    constructor(el) {
        this.DOM = {el: el};
        this.maskLat = this.DOM.el.querySelectorAll('.slider__item-mask--laterals');
        this.maskVert = this.DOM.el.querySelectorAll('.slider__item-mask--verticals');

        this.DOM.el.querySelector('a').addEventListener('click', function(e){
            e.preventDefault();
            let target = e.target.parentNode.parentNode.getAttribute('href');
            gsap.to('body', {opacity: 0, duration: .2, onComplete: function() {
                window.location.href = target
            }})
        })
    }
    enter() {
        gsap.to( this.maskLat, {width:0, duration:.5});
        gsap.to( this.maskVert, {height:0, duration:.5});
    }
    leave() {
        gsap.to( this.maskLat, {width:16, duration:.5});
        gsap.to( this.maskVert, {height:16, duration:.5});
    }
}

class AppArt {
    constructor () {
        this.el = document.querySelector('.slider');
        this.container = document.querySelector('.page-wrapper');
        this.works = [];
        this.items = [...this.el.querySelectorAll('.slider__item')];
        this.items.forEach(item => this.works.push(new ItemWorks(item)));
        this.mode = largeDevice.device;

        this.init();
    }
    init() {
        if( this.mode === 'desktop' ) {
            const cursor = new Cursor(document.querySelector('.cursor--circle'), document.querySelector('.cursor--pointer'));
            this.works.forEach((item) => {
                item.DOM.el.addEventListener('mouseenter', () => item.enter());
                item.DOM.el.addEventListener('mouseleave', () => item.leave());
            });
            this.move();
        } else {
            this.grab();
        }
        gsap.to( this.items, { 
            y: 0,
            duration: 1.5,
            ease: CustomEase.create("custom", "M0,0 C0.054,0.978 0.04,1 1,1 "),
            stagger: {
                each: 0.15
            },
        });
    }
    grab() {
        let self = this;
        const container = this.container;
        const elem = this.el;
        const position = { x: 0, y: 0 }
        
        interact(elem)
        .draggable({
            inertia: true,
            startAxis: 'x',
            lockAxis: 'x',
            modifiers: [
                interact.modifiers.restrictRect({
                    restriction: container,
                    endOnly: true
                })
            ],
            listeners: {
                move (event) {
                    var target = event.target
                    var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
            
                    gsap.to(elem, {
                        x: x,
                        duration: 1
                    });
            
                    target.setAttribute('data-x', x)
                },
            }
        }).on('tap', function (event) {
            var target = event.target;
            var link = target.closest('a');
            var src = link ? link.getAttribute('href') : null;
            if( src ){
                location.href = src
            }
        });
    }
    move() {
        let translationVal = 0;
        // get random start and end movement boundaries
        /* const xstart = getRandomNumber(15,60);
        const ystart = getRandomNumber(15,60); */
        let sliderWidth = this.el.offsetWidth;
        let containerWidth = this.container.offsetWidth;
        const xstart = (sliderWidth - containerWidth) / 2;

        const render = () => {
            translationVal = utils.lerp(translationVal, utils.map(mousepos.x, 0, containerWidth, -xstart, xstart), 0.07);

            gsap.set(this.el, {x: -translationVal});   
            requestAnimationFrame(render);
        }
        requestAnimationFrame(render);
    }
}


//Init
const largeDevice = new LargeDevice();
let mousepos = {x: largeDevice.size.width/2, y: largeDevice.size.height/2};
window.addEventListener('mousemove', ev => mousepos = utils.getMousePos(ev));
const appArt = new AppArt();