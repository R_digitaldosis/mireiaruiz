<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/favicon.png" type="image/x-icon" />
<meta property="og:title" content="Mireia Ruiz" />
<meta property="og:image" content="<?php echo esc_url( get_template_directory_uri() ); ?>/img/social-share-MR.jpg" />
<meta property="og:url" content="https://mireiaruiz.com" />
<meta property="og:description" content="This universe is full of symbolism that speaks of human progress, the empowerment of women, good and evil as unity and is focused on nature as the only truth and the passage of time as the inevitable consequence of individual and collective existence." />
<meta name="twitter:card" content="summary" />
<meta name="twitter:title" content="Mireia Ruiz" />
<meta name="twitter:description" content="This universe is full of symbolism that speaks of human progress, the empowerment of women, good and evil as unity and is focused on nature as the only truth and the passage of time as the inevitable consequence of individual and collective existence." />
<meta name="twitter:image" content="<?php echo esc_url( get_template_directory_uri() ); ?>/img/social-share-MR.jpg" />
<link rel="modulepreload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modul-utils.js">
<link rel="modulepreload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modul-initDevice.js">
<link rel="modulepreload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modul-cursor.js">
<link rel="modulepreload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modul-menu.js">
<link rel="modulepreload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/js/modul-grid.js">
<link rel="preload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/fonts/Velodroma-basic/velodroma-basic.woff2" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/fonts/Velodroma-Regular/Velodroma-Regular.woff2" as="font" type="font/woff2" crossorigin>
<link rel="preload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/fonts/MuseoSans-300/MuseoSans-300.ttf" as="font" type="font/ttf" crossorigin>
<link rel="preload" href="<?php echo esc_url( get_template_directory_uri() ); ?>/fonts/MuseoSans-900/MuseoSans-900.ttf" as="font" type="font/ttf" crossorigin>
<!--[if lt IE 9]>
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
<![endif]-->

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header class="header">
		<div class="header__logo link"><a href="/"><h1>MIREIA RUIZ</h1></a></div>
		<div class="header__menu">
			<?php
			$args = array(
				'menu'                 => '',
				'container'            => 'div',
				'container_class'      => 'menu',
				'menu_class'           => '',
				'items_wrap'           => '<ul>%3$s</ul>',
				'item_spacing'         => 'preserve',
				'add_li_class'  	   => 'link'
			);
				wp_nav_menu( $args );
			?>
			<div class="contact link">
				<?php if ( dynamic_sidebar('header-contact') ) : the_widget( 'WP_Widget_Text' ); endif; ?>
			</div>
			<div class="credits is-mobile"><a href="https://digitaldosis.com" target="_blank" rel="noreferrer">Site by DigitalDosis</a></div>
		</div>
		<button class="hamburger hamburger--spin header__icon-menu-mobile is-mobile" type="button" aria-label="menu">
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
		</button>
	</header>