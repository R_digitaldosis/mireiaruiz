<?php
/*
Template Name: About
*/

get_header(); 
$images = get_field('images');
?>
<div class="loader loading"></div>
<div class="page-wrapper page-wrapper--about page-wrapper--vertical">
    <div class="columns scroll-wrap">
        <div class="column column--left">
            <div class="column__top">
            <picture class="column__image">
                <?php
                $image_1 = esc_url($images[0]['sizes']['thumb-work-lg']);
                $image_webp_1 = $image_1.'.webp';
                ?>
                <source
                    srcset="<?= $image_webp_1 ?>"
                    type="image/webp">
                <img
                    src="<?= $image_1 ?>" alt="Studio Mireia Ruiz">
            </picture>
                <div class="column__text">
                    <h1>CV</h1>
                    <p><?=get_field('cv')?></p>
                </div>
            </div>
        </div>
        <div class="column column--center">
            <div class="column__top">
                <div class="column__text">
                    <h1>IN MY OWN WORDS</h1>
                    <p><?=get_field('about')?></p>
                </div>
            </div>
            <picture class="column__image">
                <?php
                $image_2 = esc_url($images[1]['sizes']['thumb-work-lg']);
                $image_webp_2 = $image_2.'.webp';
                ?>
                <source
                    srcset="<?= $image_webp_2 ?>"
                    type="image/webp">
                <img
                    src="<?= $image_2 ?>" alt="Work Mireia Ruiz">
            </picture>
        </div>
        <div class="column column--last">
            <div class="column__text">
                <h1>BIO</h1>
                <p><?=get_field('bio')?></p>
            </div>
        </div>
        <div class="footer__credits credits"><a href="http://digitaldosis.com" target="_blank">Site by DigitalDosis</a></div>
    </div>
</div>
<svg class="cursor cursor--pointer" width="34.5" height="10" viewBox="0 0 23.77 10">
    <circle class="cursor--pointer__point cursor--pointer__point--works cursor--pointer__point--vertical" cx="3" cy="3" r="3"/>
</svg>
<?php
get_footer();
?>