import { utils } from './modul-utils.js';

export default class Cursor {
    constructor(el, point) {
        this.DOM = {el: el, pnt: point};
        this.bounds = this.DOM.el.getBoundingClientRect();
        this.boundsPoint = this.DOM.pnt.getBoundingClientRect();
        this.links = document.querySelectorAll('.link');
        
        this.renderedStyles = {
            tx: {previous: 0, current: 0, amt: 0.2},
            ty: {previous: 0, current: 0, amt: 0.2}
        };

        this.onMouseMoveEv = () => {
            this.renderedStyles.tx.previous = this.renderedStyles.tx.current = mouse.x - this.bounds.width/2;
            this.renderedStyles.ty.previous = this.renderedStyles.ty.previous = mouse.y - this.bounds.height/2;
            this.DOM.pnt.style.opacity = 1;
            gsap.to(this.DOM.el, {duration: 0.9, ease: 'Power3.easeOut', opacity: .5});
            requestAnimationFrame(() => this.render());
            window.removeEventListener('mousemove', this.onMouseMoveEv);
        };
        window.addEventListener('mousemove', this.onMouseMoveEv);

        this.links.forEach(link => {
            link.addEventListener('mouseenter', event => {
                document.querySelector('.cursor--pointer').classList.add('is-enter');
                TweenMax.to(document.getElementById('circle-inner'), .3, {opacity: 0, transformOrigin:"50% 50%", scale: 1.8})
            })
            link.addEventListener('mouseleave', event => {
                document.querySelector('.cursor--pointer').classList.remove('is-enter');
                TweenMax.to(document.getElementById('circle-inner'), .3, {opacity: 1, transformOrigin:"50% 50%", scale: 1})
            })
        })
    }
    render() {
        this.renderedStyles['tx'].current = mouse.x - this.bounds.width/2;
        this.renderedStyles['ty'].current = mouse.y - this.bounds.height/2;

        for (const key in this.renderedStyles ) {
            this.renderedStyles[key].previous = utils.lerp(this.renderedStyles[key].previous, this.renderedStyles[key].current, this.renderedStyles[key].amt);
        }
        
        this.DOM.pnt.style.top = mouse.y - this.boundsPoint.height/2 + 'px';
        this.DOM.pnt.style.left = mouse.x - this.boundsPoint.width/2 + 'px';
        this.DOM.el.style.transform = `translateX(${(this.renderedStyles['tx'].previous)}px) translateY(${this.renderedStyles['ty'].previous}px)`;
        
        requestAnimationFrame(() => this.render());
    }
}

// Track the mouse position
let mouse = {x: 0, y: 0};
window.addEventListener('mousemove', ev => mouse = utils.getMousePos(ev));
